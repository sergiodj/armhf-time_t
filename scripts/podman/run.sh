#!/bin/sh

exec podman \
  run \
  --root=/media/sda1/adrien/containers-storage/ \
  --rm \
  -it \
  -v $PWD:$PWD \
  localhost/unstable-armhf:latest \
  env -C $PWD http_proxy=http://temp-mantic:8000 \
    ./check-armhf-time_t \
    --mode=dev \
    "$@"
