#!/bin/sh

mmdebstrap \
  --variant buildd \
  --arch armhf \
  --skip=cleanup \
  --include=eatmydata,abi-compliance-checker,dctrl-tools,procmail,apt,procps,universal-ctags,apt-file,apt-rdepends,sqlite3,iproute2,auto-apt-proxy,iputils-ping \
  --components=main \
  unstable \
| podman \
    import  \
    --root=/media/sda1/adrien/containers-storage \
    --arch armhf \
    - \
    unstable-armhf

