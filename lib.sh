SQLITE_DB="armhf-time_t-analysis.db"

log_msg() {
    local d
    d="$(date --iso-8601=seconds)"
    echo "[$d]" "$@"
}

sql_select_rowid() {
    local devpkg

    devpkg="$1"

    echo "(select rowid from packages where devpkg is \"${devpkg}\")"
}

sql_change() {
    local devpkg
    local action
    local table
    local fields
    local values

    devpkg="$1"
    action="$2"
    table="$3"
    fields="$4"
    values="$5"

    sqlite3 "${SQLITE_DB}" << EOF
.timeout 10000
${action} into ${table} (id, ${fields}) values ($(sql_select_rowid "${devpkg}"), ${values});
EOF
}

analysis_sql_result() {
    local devpkg
    local lfs
    local time_t

    devpkg="$1"
    lfs="$2"
    time_t="$3"

    log_msg "<= $devpkg: lfs=$lfs, time_t=$time_t"

    sql_change "${devpkg}" 'replace' 'analysis' 'analyzed,lfs,time_t' "true,${lfs},${time_t}"
}
